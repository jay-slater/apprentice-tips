
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="../external/css/apprentice-tips.css">
</head>
<body>

  <header id="masthead" class="site-header" itemtype="https://schema.org/WPHeader" itemscope="">
    <div class="inside-header grid-container grid-parent">
      <div class="site-logo">
        <a href="https://www.apprenticetips.com/" title="ApprenticeTips.com" rel="home">
          <img class="header-image" alt="ApprenticeTips.com" src="https://www.apprenticetips.com/wp-content/uploads/2020/02/cropped-appenticetipslogo2-1.png" title="ApprenticeTips.com">
        </a>
      </div>
    </div>
  </header>

  <nav id="site-navigation" class="main-navigation">
    <ul class="grid-container">
      <li><a href="../contact-form/">Contact Form</a></li>
      <li><a href="../apprenticeship-search/">Apprenticeships Searcher</a></li>
    </ul>
  </nav>


  <div id="main">
<?php


include('../external/php/create-form.php');
include('contact-array.php');

echo '<form action="thank-you.php" name="contact-form" method="get" class="grid-container" onsubmit="return validateForm()">'.createForm($contactArray).'
<div class="textarea">
<textarea id="comments" placeholder="Please provide any other information that you wish to let our recruiters know..." name="comments" value=""></textarea>
</div>

<div class="submit">
<input type="submit">
</div>
</form>';




?>
</div>
<script>

function validateForm() {
  var email = document.forms["contact-form"]["email"].value;
  var emailVerif = /\S+@\S+\.\S+/;
  var phone = document.forms["contact-form"]["phone"].value;
  var phoneVerif = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

  var firstname = document.forms["contact-form"]["firstname"].value;
  var surnmae = document.forms["contact-form"]["surname"].value;

  if (!emailVerif.test(email)) {
    alert("Invalid Email");
    return false;
  } else if ((email.length) > 50) {
    alert("Email too long");
    return false;
  }
  if (!phoneVerif.test(phone)) {
    alert("Invalid Contact Number");
    return false;
  } else if (phone.length > 20) {
    alert("Phone too long");
    return false;
  }
  if (firstname.length > 30) {
    alert("First name too long");
    return false;
  }
  if (surname.length > 30) {
    alert("Surname too long");
    return false;
  }
}


</script>
</body>
</html>
