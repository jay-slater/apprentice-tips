<?php

$contactArray =
[

"text" => [
  "message" => "Please enter your details and we’ll be in touch",
  "First name:" => "firstname",
  "Surname:" => "surname",
  "Email:" => "email",
  "Contact Number:" => "phone",
],
"select" => [
  "Level of previous qualification" => [
    "previous_level" => [
        0,1,2,3,4,5,6,7
      ]
  ],
],

"checkbox" => [
  "message" => "Please select the routes that interest you:",
  "Agriculture, environmental and animal care" => "agriculture",
  "Business and administration" => "business",
  "Care Services" => "care",
  "Catering and hospitality" => "catering",
  "Construction" => "construction",
  "Creative and design" => "creative",
  "Digital" => "digital",
  "Education and childcare" => "education",
  "Engineering and manafacturing" => "engineering",
  "Hair and beauty" => "hair",
  "Health and science" => "health",
  "Legal, finance and accounting" => "legal",
  "Protective services" => "protective",
  "Sales, marketing and procurement" => "sales",
  "Transport and logistics" => "transport"
]
];

?>
