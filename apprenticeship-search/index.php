
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="../external/css/apprentice-tips.css">
</head>
<body>

<header id="masthead" class="site-header" itemtype="https://schema.org/WPHeader" itemscope="">
  <div class="inside-header grid-container grid-parent">
    <div class="site-logo">
      <a href="https://www.apprenticetips.com/" title="ApprenticeTips.com" rel="home">
        <img class="header-image" alt="ApprenticeTips.com" src="https://www.apprenticetips.com/wp-content/uploads/2020/02/cropped-appenticetipslogo2-1.png" title="ApprenticeTips.com">
      </a>
    </div>
  </div>
</header>

<nav id="site-navigation" class="main-navigation">
  <ul class="grid-container">
    <li><a href="../contact-form/">Contact Form</a></li>
    <li><a href="../apprenticeship-search/">Apprenticeships Searcher</a></li>
  </ul>
</nav>


<div id="main">
<?php


include('../external/php/create-form.php');
include('apprenticeship-searcher-array.php');

echo '<form method="post" class="grid-container apprentice-searcher">'.createForm($appSearchArray).'
<div class="submit">
<input type="submit" value="Find Apprenticeships">
</div>
</form>';

if(isset($_POST['route'])){
  echo '<div class="grid-container table"><div class="inner-div"><p>You searched for the following:</p>';
  foreach ($_POST as $key => $value) {
    echo '<p>'.$key.': '.$value.'</p>';
  }

  $servername = "ap-sp-proj-c.chwmwhavjzga.eu-west-2.rds.amazonaws.com";
  $port = 3306;
  $socket = "";
  $username = "admin";
  $password = "Yh2RCmGWfXFxwZv68J4m";
  $dbname = "webform";

  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);

  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  $route = $_POST['route'];
  $status = $_POST['status'];
  $level = $_POST['level'];
  $duration = $_POST['duration'];


  $sql = "SELECT name, link FROM Apprenticeships WHERE route='$route'";
  if($status != 'Any'){
    $sql .= 'AND status="'.$status.'"';
  }
  if($level != 'Any'){
    $sql .= 'AND level='.$level;
  }
  if($duration != 'Any'){
    $sql .= 'AND duration='.$duration;
  }

  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    echo '<table><tr><th>Name</th><th>Link</th></tr>';
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["name"]."</td> <td><a href=".$row["link"]." target='_blank'>".$row["link"]."</a></td></tr>";
    }
    echo '</table>';
  } else {
    echo "0 results";
  }

  // Closes connection
  $conn->close();
  echo "</div></div>";
}



?>
</div>
</body>
</html>
