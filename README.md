# README #

This is an application with two pages, contact form and apprenticeship search

# Contact Form #

This will allow any interested persons to submit data about themselves and any interest in apprenticeships

This will include the following:

* Text box for First Name, Surname, Email and Contact Number

* Dropdown for level of previous qualification

* Tick boxes for interested routes

* Text area for additional information

* Validation of all inputs and Captcha to ensure only humans input data

# Apprenticeship search #

This will allow recruitment staff to search for apprenticeships

This will include the following:

* Drop downs for Route, Status, Level and Duration

* Once Find apprenticeships button is pressed a list of apprenticeships will appear that include the relevant inputted search data
