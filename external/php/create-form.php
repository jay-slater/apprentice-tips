<?php



function createForm($formArray){

foreach ($formArray as $type => $typeData) {
  if ($type == "select") {
    foreach ($typeData as $title => $value) {
      if($title == "message"){
        $form .= '<p><b>'.$value.'</b></p>';
      }
      // Create label and initialise select input

      foreach ($value as $name => $options) {
        $form .= '
        <div class='.$type.'>';
        $form .= '
        <label for='.$name.'>'.$title.'</label>
        <select name='.$name.'>';
        foreach ($options as $option) {
          $form .= '
          <option value="'.$option.'">'.$option.'</option>
          ';
        }
        $form .= '</select></div>';
      }
    }
  } elseif ($type == "text") {
      foreach ($typeData as $title => $value) {
        if($title == "message"){
          $form .= '<p><b>'.$value.'</b></p>';
        } else {
          $form .= '
          <div class='.$type.'>
            <label for='.$value.'>'.$title.'</label>
            <input type='.$type.' id='.$value.' name='.$value.' required>
          </div>
          ';
        }
      }
  } elseif ($type == "checkbox") {
      foreach ($typeData as $title => $value) {
        if($title == "message"){
          $form .= '<p><b>'.$value.'</b></p>';
        } else {
          $form .= '
          <div class='.$type.'>
            <label for='.$value.'>'.$title.'</label>
            <input type='.$type.' id='.$value.' name='.$value.' value="1">
          </div>
          ';
        }
      }
  }
}

return $form;
}
?>
