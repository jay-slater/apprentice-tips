<?php

$appSearchArray =
[
  "select" => [
    "Route:" => [
      "route" => [
        "Agriculture, environmental and animal care",
        "Business and administration",
        "Care services",
        "Catering and hospitality",
        "Construction",
        "Creative and design",
        "Digital",
        "Education and childcare",
        "Engineering and manufacturing",
        "Hair and beauty",
        "Health and science",
        "Legal, finance and accounting",
        "Protective services",
        "Sales, marketing and procurement",
        "Transport and logistics"
      ]
    ],
  "Status:" => [
    "status" => [
      "Any",
      "Approved for delivery",
      "In development",
      "Retired",
      "Withdrawn",
      "Proposal in development"
    ]
  ],
  "Level:" => [
    "level" => [
      "Any",2,3,4,5,6,7
    ]
  ],
  "Duration" => [
    "duration" => [
      "Any","0","12","13","14","15","16","18","21","22","24","26","27","30","33","34","36","38","42","48","54","60","66"
    ]
  ]
]
];

?>
